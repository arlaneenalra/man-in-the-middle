var httpProxy = require('http-proxy'),
  http = require('http'),
  url = require('url'),
  net = require('net');

module.exports = function (pipe) {
  var proxy = new httpProxy.RoutingProxy(),
    pipeWrapper = function (type, req, res) {
      pipe && pipe(type, req, res);
    };

  // error handling
  proxy.on('proxyError', function (e, req, res) {
    console.log('502 - Bad Gateway' );
    pipeWrapper('error', req, res);

    if (res.writeHead) {
      res.writeHead(502, {
        'Content-Type': 'text/plain'
      });

      res.end("Bad Gateway - Unable to process request.");
    } else {
      res.write("HTTP/1.1 502 Bad Gateway\r\n\r\n");
    }
  });

  var server = http.createServer(function (req, res) {
    console.log(req.method + ' ' + req.url);

    var parts = url.parse(req.url);

    parts.port = parts.port === null ? 80 : parts.port;
    console.log(parts);
    
    pipeWrapper('http', req, res);

    proxy.proxyRequest(req, res, {
      host: parts.hostname,
      port: parts.port,
    });
  });


  // handle ssl and other connections
  server.on('connect', function (req, socket, head) {
    console.log(req.method + ' ' + req.url);

    // URL is in the form 'hostname:port'
    var parts = req.url.split(':', 2);
      
    // open a TCP connection to the remote host
    var conn = net.connect(parts[1], parts[0], function() {
      // respond to the client that the connection was made
      socket.write("HTTP/1.1 200 OK\r\n\r\n");

      // create a tunnel between the two hosts
      socket.pipe(conn);
      conn.pipe(socket);
    });

    // propagate the error upstream
    conn.on('error', function (e) {
      proxy.emit('proxyError', e, req, socket);
    });
  });

  return server;
};
