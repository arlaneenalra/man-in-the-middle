var proxyFactory = require('./lib/proxy'),
  express = require('express');

var app = express()
  , server = require('http').createServer(app)
  , io = require('socket.io').listen(server);

// setup static file serving
app.use(express.static(__dirname + '/public'));

var clients = [];

// setup socket connection
io.sockets.on('connection', function (socket) {
  socket.emit('setup', { hello: 'world' });
});

server.listen(9090);

proxy = proxyFactory(function (type, req, res) {
  io.sockets.emit('request', {
    type: type,
    url: req.url,
    method: req.method,
    headers: req.headers
  });
});

proxy.listen(8080, function () {
  console.log('Listening on port 8080');
});

